﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace mergecatelog.core.Csv
{
    internal class CsvContentReader : ICsvContentReader
    {
        public List<T> Read<T>(string path)
        {
            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.Delimiter = ",";
                csv.Read();
                csv.ReadHeader();
                return csv.GetRecords<T>().ToList();
            }
        }
    }
}
