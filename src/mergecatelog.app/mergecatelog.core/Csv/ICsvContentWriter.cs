﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Csv
{
    public interface ICsvContentWriter
    {
        void Write<T>(List<T> content, string dst);
    }
}
