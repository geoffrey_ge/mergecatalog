﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Csv
{
    public interface ICsvContentReader
    {
        List<T> Read<T>(string path);
    }
}
