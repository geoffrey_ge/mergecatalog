﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace mergecatelog.core.Csv
{

    internal class CsvContentWriter : ICsvContentWriter
    {
        public void Write<T>(List<T> content,string dst)
        {
            using (var writer = new StreamWriter(dst,false))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(content);
            }
        }
    }
}
