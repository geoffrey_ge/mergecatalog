﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Comparer
{
    public class OutputItemDescriptionComparer : IComparer<OutputItem>
    {
        public int Compare(OutputItem x, OutputItem y)
        {
            return String.Compare(x.Description, y.Description);
        }
    }
}
