﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Comparer
{
    public class JoinedItemSkuComparer : IComparer<JoinedItem>
    {
        public int Compare(JoinedItem x, JoinedItem y)
        {
            return String.Compare(x.SKU, y.SKU);
        }
    }
}
