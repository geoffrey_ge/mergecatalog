﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Settings
{
    public class Context
    {
        public string Folder { get; set; }
        public string OutputFileName { get; set; }

        public string OutputFilePath => this.Folder + "/" + this.OutputFileName;
    }
}
