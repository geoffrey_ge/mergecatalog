﻿using mergecatelog.core.Csv;
using mergecatelog.core.Service;
using mergecatelog.core.Settings;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;


[assembly: InternalsVisibleTo("mergecatelog.test")]
namespace mergecatelog.core.Ioc
{
    public static class BindingModule
    {
        public static IServiceCollection RegisterCore(this IServiceCollection services)
        {
            services.AddSingleton<ICsvContentReader, CsvContentReader>()
                    .AddSingleton<ICsvContentWriter, CsvContentWriter>()
                    .AddSingleton<ICatalogService, CatalogService>()
                    .AddSingleton<Context>();

            return services;
        }
    }
}
