﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Entity
{
    public class BarcodeItem
    {
        public int SupplierID { get; set; }
        public string SKU { get; set; }
        public string Barcode { get; set; }
    }
}
