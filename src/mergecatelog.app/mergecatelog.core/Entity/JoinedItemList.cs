﻿using mergecatelog.core.Comparer;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mergecatelog.core.Entity
{
    public class JoinedItemList
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public List<JoinedItem> Items { get; set; }

        public JoinedItemList()
        {
            Items = new List<JoinedItem>();
        }

        public JoinedItemList(List<JoinedItem> items)
        {
            Items = items;
        }

        internal object FirstOrDefault(object xunit)
        {
            throw new NotImplementedException();
        }

        public void Merge(JoinedItemList otherItems)
        {
            otherItems.SortBySku();

            //indicate whether the item already existing in the list already, by compare barcode
            bool exists = false;
            string sku = null;
            int i = 0;
            List<JoinedItem> tempItems = null; 

            while(i < otherItems.Count)
            {
                exists = false;
                sku = otherItems[i].SKU;
                int j = i;
                tempItems = new List<JoinedItem>();

                //check any barcode for the other item(same sku), exists in this item list
                for (; j< otherItems.Count; j++)
                {
                    if (!otherItems[j].SKU.Equals(sku))
                        break;

                    tempItems.Add(otherItems[j]);

                    //find item by barcode in this item list
                    var item = this.Items.FirstOrDefault(x => x.Barcode.Equals(otherItems[i].Barcode));

                    //found 
                    if (item != null)
                    {
                        exists = true;
                    }
                }

                //not exists, add all the other items(same sku) to this item list 
                if (!exists)
                {
                    this.Items.AddRange(tempItems);
                }

                //exists, skip all the other items(same sku)

                //move to the beginning of next sku
                i = j;
            }
        }

        public bool Delete(string barcode)
        {
            //find item by barcode
            var deleteSkus = this.Items.Where(x => x.Barcode.Equals(barcode)).Select(x => x.SKU).Distinct();

            if(!deleteSkus.Any())
            {
                _logger.Warn($"No Item found for barcode {barcode}");
                return false;
            }

            //remove all the items that matches skus
            this.Items.RemoveAll(x => deleteSkus.Contains(x.SKU));

            return true;
        }

        public bool Add(JoinedItem item)
        {
            //check whether the barcode already exist
            if (this.Items.Any(x => x.Barcode.Equals(item.Barcode)))
            {
                _logger.Warn($"Item already exists for barcode {item.Barcode}");
                return false;
            }

            this.Items.Add(item);

            return true;
        }

        public void Update(JoinedItem item)
        {
            //not sure about this requirement. need further confirmation.
            //requirement: An existing product in Catalog B got new supplier with set of barcodes
            //answer: 5. what are the expected input for removing or updating record in BAU mode? barcode or sku? or i can specify myself? – Barcode
            throw new NotImplementedException();
        }

        public List<OutputItem> GenerateOutputList()
        {
            var list = this.Items.Select(x => new OutputItem(x.SKU, x.Description, x.Source)).Distinct().ToList();
            list.Sort(new OutputItemDescriptionComparer());

            return list;
        }

        public void SortBySku()
        {
            this.Items.Sort(new JoinedItemSkuComparer());
        }

        public JoinedItem this[int index]
        {
            get
            {
                return this.Items[index];
            }
        }

        public int Count => this.Items.Count;
    }
}
