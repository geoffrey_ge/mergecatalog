﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Entity
{
    public class CatalogItem
    {
        public string SKU { get; set; }
        public string Description { get; set; }
    }
}
