﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Entity
{
    public class JoinedItem
    {
        public string SKU { get; set; }
        public string Supplier { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Barcode { get; set; }

        public JoinedItem() { }

        public JoinedItem(string sku, string supplier, string description, string barCode, string source)
        {
            this.SKU = sku;
            this.Supplier = supplier;
            this.Description = description;
            this.Source = source;
            this.Barcode = barCode;
        }
    }
}
