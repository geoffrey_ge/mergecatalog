﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Entity
{
    public class OutputItem: IEquatable<OutputItem>
    {
        public string SKU { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public OutputItem() { }
        public OutputItem(string sku, string description, string source)
        {
            this.SKU = sku;
            this.Description = description;
            this.Source = source;
        }

        public bool Equals(OutputItem other)
        {
            return this.SKU.Equals(other.SKU, StringComparison.InvariantCultureIgnoreCase)
                && this.Description.Equals(other.Description, StringComparison.InvariantCultureIgnoreCase)
                && this.Source.Equals(other.Source, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            OutputItem other = obj as OutputItem;

            if (other == null)
                return false;

            return Equals(other);
        }

        public override int GetHashCode()
        {
            return (this.SKU + this.Description + this.Source).GetHashCode();
        }
    }
}
