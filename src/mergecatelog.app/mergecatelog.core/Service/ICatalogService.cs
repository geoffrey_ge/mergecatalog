﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.core.Service
{
    public interface ICatalogService
    {
        JoinedItemList Merge(string source1, string source2);
        void Save(List<OutputItem> outputItems);
    }
}
