﻿using mergecatelog.core.Csv;
using mergecatelog.core.Entity;
using mergecatelog.core.Settings;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace mergecatelog.core.Service
{
    internal class CatalogService : ICatalogService
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private ICsvContentReader _reader = null;
        private ICsvContentWriter _writer = null;
        private Context _context = null;

        public CatalogService(Context context, ICsvContentReader reader, ICsvContentWriter writer)
        {
            _context = context;
            _reader = reader;
            _writer = writer;
        }

        public JoinedItemList Merge(string source1, string source2)
        {
            var itemList1 = this.GetSourceItemList(source1);
            var itemList2 = this.GetSourceItemList(source2);

            itemList1.Merge(itemList2);
            return itemList1;
        }

        public void Save(List<OutputItem> outputItems)
        {
            _writer.Write<OutputItem>(outputItems, _context.OutputFilePath);
        }

        private JoinedItemList GetSourceItemList(string source)
        {
            var barcodeItems = _reader.Read<BarcodeItem>(_context.Folder + "/barcodes" + source + ".csv");
            var catalogItems = _reader.Read<CatalogItem>(_context.Folder + "/catalog" + source + ".csv");
            var supplierItems = _reader.Read<SupplierItem>(_context.Folder + "/suppliers" + source + ".csv");

            //catalogItems.Join(barcodeItems,)
            var items = (from c in catalogItems
                         join b in barcodeItems on c.SKU equals b.SKU
                         join s in supplierItems on b.SupplierID equals s.ID
                         select new JoinedItem(c.SKU,s.Name,c.Description,b.Barcode,source)
                         );

            return new JoinedItemList(items.ToList());
        }
    }
}
