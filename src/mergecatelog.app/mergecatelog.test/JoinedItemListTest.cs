﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace mergecatelog.test
{
    public class JoinedItemListTest
    {
        public JoinedItemListTest()
        {

        }

        [Fact]
        public void Merge()
        {
            var srcA = PrepareSrcA();
            var srcB = PrepareSrcB();

            srcA.Merge(srcB);

            Assert.Equal(10, srcA.Count);
            Assert.Null(srcA.Items.FirstOrDefault(x => x.SKU.Equals("sku-b1")));
            Assert.Equal(4, srcA.Items.Where(x => x.Description.Equals("des-1")).Count());
            Assert.Single(srcA.Items.Where(x => x.SKU.Equals("sku-b3")));
            Assert.Equal(4, srcA.Items.Where(x => x.Description.Equals("des-2")).Count());
            Assert.Equal(2, srcA.Items.Where(x => x.SKU.Equals("sku-a2")).Count());
        }

        [Fact]
        public void GenerateOutput()
        {
            var srcA = PrepareSrcA();
            var outputs = srcA.GenerateOutputList();

            Assert.Equal(3, outputs.Count);
        }

        [Fact]
        public void GenerateMergedOutput()
        {
            var srcA = PrepareSrcA();
            var srcB = PrepareSrcB();

            srcA.Merge(srcB);

            var outputs = srcA.GenerateOutputList();

            Assert.Equal(5, outputs.Count);
        }

        [Fact]
        public void Add_Existing()
        {
            var srcA = PrepareSrcA();
            var item = new JoinedItem("sku-b1", "supplier-b1", "des-1", "barcode-a11", "B");

            bool r = srcA.Add(item);
            Assert.Equal(7, srcA.Count);
            Assert.False(r);
        }

        [Fact]
        public void Add_NonExisting()
        {
            var srcA = PrepareSrcA();
            var item = new JoinedItem("sku-b2", "supplier-a2", "des-2", "barcode-b21", "B");

            bool r = srcA.Add(item);
            Assert.Equal(8, srcA.Count);
            Assert.True(r);
        }

        [Fact]
        public void Delete_Existing()
        {
            var srcA = PrepareSrcA();
            bool r = srcA.Delete("barcode-a14");

            Assert.Equal(3, srcA.Count);
            Assert.True(r);

            r = srcA.Delete("barcode-a3");
            Assert.Equal(2, srcA.Count);
            Assert.True(r);
        }

        [Fact]
        public void Delete_NonExisting()
        {
            var srcA = PrepareSrcA();
            bool r = srcA.Delete("barcode-a4");
            Assert.Equal(7, srcA.Count);
            Assert.False(r);
        }

        private JoinedItemList PrepareSrcA()
        {
            var src = new JoinedItemList();

            src.Items.Add(new JoinedItem("sku-a1","supplier-a1","des-1","barcode-a11","A"));
            src.Items.Add(new JoinedItem("sku-a1", "supplier-a1", "des-1", "barcode-a12", "A"));
            src.Items.Add(new JoinedItem("sku-a1", "supplier-a1", "des-1", "barcode-a13", "A"));
            src.Items.Add(new JoinedItem("sku-a1", "supplier-a1", "des-1", "barcode-a14", "A"));

            src.Items.Add(new JoinedItem("sku-a2", "supplier-a2", "des-2", "barcode-a21", "A"));
            src.Items.Add(new JoinedItem("sku-a2", "supplier-aa2", "des-2", "barcode-aa21", "A"));
            src.Items.Add(new JoinedItem("sku-a3", "supplier-a3", "des-3", "barcode-a3", "A"));

            return src;
        }

        private JoinedItemList PrepareSrcB()
        {
            var src = new JoinedItemList();

            src.Items.Add(new JoinedItem("sku-b1", "supplier-b1", "des-1", "barcode-a11", "B"));
            src.Items.Add(new JoinedItem("sku-b1", "supplier-b1", "des-1", "barcode-a12", "B"));

            src.Items.Add(new JoinedItem("sku-b2", "supplier-a2", "des-2", "barcode-b21", "B"));
            src.Items.Add(new JoinedItem("sku-b2", "supplier-aa2", "des-2", "barcode-bb21", "B"));
            src.Items.Add(new JoinedItem("sku-b3", "supplier-a3", "des-b3", "barcode-b3", "B"));

            return src;
        }
    }
}
