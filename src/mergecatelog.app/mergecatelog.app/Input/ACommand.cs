﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.app.Input
{
    public abstract class ACommand
    {
        public abstract string Key { get; }

        public bool Match(string inputKey)
        {
            return inputKey.Equals(Key, StringComparison.InvariantCultureIgnoreCase);
        }

        public abstract bool Parse(string[] paramters);
    }

    public abstract class AInitialCommand : ACommand { }

    public abstract class ABauCommand:ACommand
    {
        public abstract bool Execute(JoinedItemList items);
    }

}
