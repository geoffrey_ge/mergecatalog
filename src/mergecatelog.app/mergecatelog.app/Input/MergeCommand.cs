﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace mergecatelog.app.Input
{
    public class MergeCommand : AInitialCommand
    {
        public string Folder { get; private set; }
        public string Source1 { get; private set; }
        public string Source2 { get; private set; }

        public override string Key => "merge";

        public override bool Parse(string[] paramters)
        {
            if (!Validate(paramters))
                return false;

            Folder = paramters[1];
            Source1 = paramters[2];
            Source2 = paramters[3];

            return true;
        }

        private bool Validate(string[] paramters)
        {
            if (paramters.Length < 4)
                return false;

            if (!Directory.Exists(paramters[1]))
                return false;

            return true;
        }
    }
}
