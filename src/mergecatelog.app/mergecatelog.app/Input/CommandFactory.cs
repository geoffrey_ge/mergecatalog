﻿using mergecatelog.core.Entity;
using mergecatelog.core.Settings;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mergecatelog.app.Input
{
    internal class CommandFactory : ICommandFactory
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private IEnumerable<ABauCommand> _bauCommands = null;
        private IEnumerable<AInitialCommand> _initialCommands = null;

        public CommandFactory(IEnumerable<ABauCommand> bauCommands,IEnumerable<AInitialCommand> initialCommands)
        {
            _bauCommands = bauCommands;
            _initialCommands = initialCommands;
        }

        public ABauCommand BuildBauCommand(string parameter)
        {
            return this.FindCmd(this._bauCommands, parameter);
        }

        public AInitialCommand BuildInitialCommand(string parameter)
        {
            return  this.FindCmd(this._initialCommands, parameter);
        }

        private T FindCmd<T>(IEnumerable<T> cmds,string parameter) where T:ACommand
        {
            var parameters = parameter.Split(' ');
            var cmd = cmds.FirstOrDefault(x => x.Match(parameters[0]));

            if (cmd == null)
            {
                _logger.Warn($"No command match with {string.Join(" ", parameters)}");
                return null;
            }

            if (!cmd.Parse(parameters))
            {
                _logger.Warn($"Invalid command {string.Join(" ", parameters)}");
                return null;
            }

            return cmd;
        }
    }
}
