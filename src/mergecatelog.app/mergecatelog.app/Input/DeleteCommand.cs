﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.app.Input
{
    public class DeleteCommand : ABauCommand
    {
        public string Barcode { get; set; }

        public override string Key => "delete";

        public override bool Execute(JoinedItemList items)
        {
            return items.Delete(this.Barcode);
        }

        public override bool Parse(string[] paramters)
        {
            if (!Validate(paramters))
                return false;

            Barcode = paramters[1];

            return true;
        }

        private bool Validate(string[] paramters)
        {
            if (paramters.Length < 2)
                return false;

            return true;
        }
    }
}
