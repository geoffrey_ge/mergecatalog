﻿using mergecatelog.core.Entity;
using mergecatelog.core.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.app.Input
{
    public interface ICommandFactory
    {
        ABauCommand BuildBauCommand(string parameter);

        AInitialCommand BuildInitialCommand(string parameter); 
    }
}
