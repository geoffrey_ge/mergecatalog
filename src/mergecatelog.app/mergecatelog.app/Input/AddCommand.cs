﻿using mergecatelog.core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace mergecatelog.app.Input
{
    public class AddCommand : ABauCommand
    {
        public string SKU { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Barcode { get; set; }
        public string Supplier { get; set; }

        public override string Key => "add";

        public override bool Execute(JoinedItemList items)
        {
           return items.Add(new JoinedItem(SKU, Supplier, Description, Barcode, Source));
        }

        public override bool Parse(string[] parameters)
        {
            if (!Validate(parameters))
                return false;

            SKU = parameters[1];
            Description = parameters[2];
            Source = parameters[3];
            Barcode = parameters[4];
            Supplier = parameters[5];

            return true;
        }

        private bool Validate(string[] paramters)
        {
            if (paramters.Length < 6)
                return false;

            return true;
        }
    }
}
