﻿using mergecatelog.core.Ioc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using NLog;
using mergecatelog.app.Input;
using mergecatelog.core.Settings;
using Microsoft.Extensions.Configuration;
using mergecatelog.core.Service;
using mergecatelog.core.Entity;

namespace mergecatelog.app
{
    class Program
    {
        static Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        static IConfiguration Config = null;
        static ServiceProvider ServiceProvider = null;


        static void Main(string[] args)
        {
            try
            {
                Startup();

                var context = ServiceProvider.GetService<Context>();
                context.OutputFileName = Config.GetValue<string>("OutputFileName");

                var inputFactory = ServiceProvider.GetService<ICommandFactory>();
                var service = ServiceProvider.GetService<ICatalogService>();
                string cmdStr = null;

                //all bau will operate on this object
                JoinedItemList mergedCatalog = null;

                Console.WriteLine("Start of Merge Catalog Program. Please note that it doesnt support space within parameter!");
                Console.WriteLine();

               Console.WriteLine(@"Please Merge Catalogs first. Merge [folder] [source1] [source2]      like: merge /input A B");
                do
                {
                    cmdStr = Console.ReadLine();
                    var cmd = inputFactory.BuildInitialCommand(cmdStr);
                    if(cmd is MergeCommand)
                    {
                        var mergedCmd = (MergeCommand)cmd;
                        context.Folder = mergedCmd.Folder;

                        Console.WriteLine($"Merging Catalog {mergedCmd.Source1} {mergedCmd.Source2}...");
                        mergedCatalog = service.Merge(mergedCmd.Source1, mergedCmd.Source2);
                        service.Save(mergedCatalog.GenerateOutputList());

                        Console.WriteLine($"Merged and Saved!");

                        break;
                    }

                } while (true);

                Console.WriteLine();
                Console.WriteLine("     1. Delete [barcode]                                             like: Delete s7013910076253");
                Console.WriteLine("     2. Add [sku] [description] [source] [barcode] [supplier]        like: Add sku-z description-z A zxce896 supplier-z");
                Console.WriteLine("     3. EXIT for end");
                do
                {
                    cmdStr = Console.ReadLine();
                    if (cmdStr.Equals("EXIT", StringComparison.InvariantCultureIgnoreCase))
                    {
                        break;
                    }

                    var cmd = inputFactory.BuildBauCommand(cmdStr);
                    if(cmd == null)
                    {
                        Console.WriteLine($"Invalid Command {cmdStr}");
                        Console.WriteLine();
                        continue;
                    }

                    var result = cmd.Execute(mergedCatalog);

                    if(result)
                        Console.WriteLine($"{cmd.Key.ToUpper()} Success!");

                    Console.WriteLine();

                } while (true);

                Console.WriteLine("Saving....");

                service.Save(mergedCatalog.GenerateOutputList());

                Console.WriteLine("Saved!");


            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Internal Error !!!!");
            }
        }

        private static void Startup()
        {
            Config = new ConfigurationBuilder()
                          .SetBasePath(Directory.GetCurrentDirectory())
                          .AddJsonFile("appsettings.json", optional: false)
                          .Build();

            var sc = new ServiceCollection();
            sc.RegisterCore();

            sc.AddSingleton<ICommandFactory, CommandFactory>();

            sc.AddTransient<ABauCommand, DeleteCommand>()
                .AddTransient<ABauCommand, AddCommand>()
                .AddTransient<AInitialCommand, MergeCommand>();

            ServiceProvider = sc.BuildServiceProvider();
        }
    }
}
