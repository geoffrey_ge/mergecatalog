dotnet restore ./src/mergecatelog.app/mergecatelog.app.sln --source "https://api.nuget.org/v3/index.json"
dotnet build ./src/mergecatelog.app/mergecatelog.app.sln
dotnet publish ./src/mergecatelog.app/mergecatelog.app/mergecatelog.app.csproj -c Release -o ./artifacts

cd ./artifacts
mergecatelog.app.exe