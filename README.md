## Note
---
1. Just did simple input parameter validation. The program doesn't support space inside parameter.
2. Implemented Merge, Add, Delete. Update not implemented, because i m not sure about the requirement, can be impletement once confirmed.
3. All the operations are executed on in-memory data structure. And save to output.csv at the end. Can discuss furthur if memory optimization is required.
4. Unit test just covered the core object, JoinedItemList. 



## How to Run
---

1. **With Docker**
	
	```docker
	$ docker build -t geoff/mergecatalog -f Dockerfile .
	```
	
	mount input folder onto docker container volumne
	
	```docker
	$ docker run -i -v {Local Input Folder}:/input geoff/mergecatalog dotnet mergecatelog.app.dll
	```
  
	*Running Result*
	![console](img/Console.jpg?raw=true)
	![output](img/Output.jpg)
	
	
2. ** Or with .net core runtime 3.1 intalled**
	
	 
	```
	just double click the run.bat, note that the context path is ./artifacts
	```
	
	*Running Result*
	![dotnet run](img/run.bat.jpg)
